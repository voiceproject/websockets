//pom.xml hérite du parent spring-boot-starter-parent qui nous permet de ne plus nous
// soucier des versions des dépendances et de leur compatibilité
//CE SPRINGBOOT Tomcat : intégré, va nous permettre de lancer notre application en
// exécutant tout simplement le jar sans avoir à le déployer dans un serveur d'application.
package projet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import projet.config.Config;
import springfox.documentation.spring.web.json.Json;

import java.io.IOException;

@SpringBootApplication
public class MyApplication {

    public MyApplication() throws IOException, InterruptedException {

    }

    public static void main(String[] args) {
        SpringApplication.run(MyApplication.class, args);
    }
    ChatServer chatServer = new ChatServer();

}
