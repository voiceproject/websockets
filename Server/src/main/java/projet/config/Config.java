package projet.config;

public class Config {
    public Config() {
    }


    public String urlApiRemy = "http://51.91.102.237:4000/sentences";
    public int port = 8887;

    public String getUrlApiRemy() {
        return urlApiRemy;
    }

    public void setUrlApiRemy(String urlApiRemy) {
        this.urlApiRemy = urlApiRemy;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }


}
