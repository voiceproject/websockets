# [WEBSOCKET - JAVA]
* **1.Introduction**
* **2.Installation**
* **3.Server WebSocket**
* **4.Client WebSocket**
* **5.Docker Server**


# Introduction

Repository recensant l'ensemble des fichiers nécessaire au bon fonctionnement du WEBSOCKET en JAVA. L'objectif de cette partie est de transferet via le protocol WebSocket, une ou plusieurs phrase entre un client WebSocket et un server Websocket, puis de poster la phrase avec une requete Post pour une API. La phrase sera dans le Body sous format JSON avec le nom `sentence`.
```json
{
    "sentence":"la phrase que vous aller choisir"
}
```

Prérequis ---- Dernière version de JAVA à jour
        Ici pour l'installer :
        
Linux --> " sudo apt-get install openjdk-7-jdk "

Windows --> https://www.java.com/fr/download/

# Installation et utilisation

Créer un dossier dans lequel nous allons mettre tout nos fichiers, client, server ...

## Activer le serveur

Déplacez vous dans le dossier `Server`.
--> Pour trouver les sources, vous pouvez vous déplacer ici: 

    ```ruby
    cd src\main\java\projet\
     ```
    Dans le package `config`, vous pouvez retrouvez la classe `Config.java` où toutes les configurations du serveur se trouvent. Nottament le 'port' ansi que `l'url` de l'API où poster le JSON. Vous pouvez les modifier à votre guise selon vos besoins.
    Sinon, vous pouvez retrouvez le `.JAR` exécutable dans le dossier suivant:
    
    ```ruby
    cd target\
    ```
    Vous pouvez exécuter et lancer le serveur avec cette commande-ci :
    ```ruby
    java -jar JAVAServerWebSocket.jar
    ```


## Activer le client

Déplacez vous dans le dossier `Client`.
--> Pour trouver les sources, vous pouvez vous déplacer ici: 

    ```
    cd src\main\java\projet\
    ```
    Dans le package `config`, vous pouvez retrouvez la classe `Config.java` où toutes les configurations du serveur se trouvent. Nottament `l'url` du Serveur WebSocket. Vous pouvez les modifier à votre guise selon vos besoins.
    Sinon, vous pouvez retrouvez le `.JAR` exécutable dans le dossier suivant:
    ```
    cd client\target\
    ```
    Vous pouvez exécuter et lancer le serveur avec cette commande-ci :
    ```
    java -jar JAVAClientWebSocket.jar La phrase que vous voulez transmettre
    ```
    Ici la phrase que vous voulez transmettre se récupère grâce aux arguments passés en ligne de commande. Cette phrase sera ensuite transmise au Serveur WebSocket, puis à l'API.


## Dockeriser le server

Builder l'image du server java recevant les textes retranscrits, à l'aide du Dockerfile et de cette commande en se placant à la racine du projet :
`docker build -t server-java-text .`

Ensuite lancer le conteneur de l'application :
`docker run -d --restart=always -p 8887:8887 -v "$PWD":/usr/servertext --name server-java-text java-server-text`

