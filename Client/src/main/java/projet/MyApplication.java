//pom.xml hérite du parent spring-boot-starter-parent qui nous permet de ne plus nous
// soucier des versions des dépendances et de leur compatibilité
//CE SPRINGBOOT Tomcat : intégré, va nous permettre de lancer notre application en
// exécutant tout simplement le jar sans avoir à le déployer dans un serveur d'application.
package projet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import projet.config.Config;

import java.net.URISyntaxException;

@SpringBootApplication
public class MyApplication {
    public static void main(String[] args) throws URISyntaxException {

        SpringApplication.run(MyApplication.class, args);
        Config config = new Config();
        System.out.println("Le client WebSocket est en écoute sur l'url' : " +config.getUrlServerWebSocket() +". Attente d'une phrase à envoyer.");
        System.out.println("Le client Filer est en écoute sur le port : "+ config.getPortServerSendFiler()+"  et à l'addresse suivante : "+ config.getIpServerSendFiler()+"  Attente d'un fichier à envoyer à l'emplacement : "+ config.getLocationPackageFiles()+".");
/*

        //ici appeller soi même
        String json = "{ \"sentence\": \""+"Ici une phrase de test à envoyer au server"+"\"}";
        StringBuffer myRetBuf = new StringBuffer("");
        myRetBuf.append(json);
        Json myJsonObj = new Json(myRetBuf.toString());
        RestTemplate myRestTemplate = new RestTemplate();
        ResponseEntity<String> result;
        HttpEntity<Json> request = new HttpEntity<>(myJsonObj);
        result = myRestTemplate.exchange("http://localhost:9090/api/v1/sentence", HttpMethod.POST, request, String.class);

         /*
        Config config = new Config();

        System.out.println("Args Number: " + args.length);
        String sendText = "";
        for(int i = 0; i < args.length; ++i) {
            System.out.println("Arg " + i + ": " + args[i]);
            sendText = sendText + " "+ args[i];
        }
        try{
            Client c = new Client(new URI( config.getUrlServer() ) );
            c.makeConnection(sendText);
        }catch (Exception e){
            System.err.println(e);
            System.exit(1);
        }

         */
    }

}
