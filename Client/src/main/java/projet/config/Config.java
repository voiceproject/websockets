package projet.config;

public class Config {
    public Config() {
    }

    public String urlServerWebSocket = "http://51.91.102.237:8887";
    int portServerSendFiler = 8888;
    String ipServerSendFiler = "51.91.102.237";
    String locationPackageFiles = "/home/pi/voiceproject/websockets/Client/fileToSend/";


    public String getLocationPackageFiles() {
        return locationPackageFiles;
    }

    public void setLocationPackageFiles(String locationPackageFiles) {
        this.locationPackageFiles = locationPackageFiles;
    }



    public int getPortServerSendFiler() {
        return portServerSendFiler;
    }

    public void setPortServerSendFiler(int portServerSendFiler) {
        this.portServerSendFiler = portServerSendFiler;
    }

    public String getIpServerSendFiler() {
        return ipServerSendFiler;
    }

    public void setIpServerSendFiler(String ipServerSendFiler) {
        this.ipServerSendFiler = ipServerSendFiler;
    }


    public String getUrlServerWebSocket() {
        return urlServerWebSocket;
    }

    public void setUrlServerWebSocket(String urlServerWebSocket) {
        this.urlServerWebSocket = urlServerWebSocket;
    }


}
