package projet.dao.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import projet.service.ClientWebSocket;
import projet.config.Config;
import projet.form.JsonForm;
import projet.form.JsonFormFile;
import projet.service.ClientFileSend;
import projet.service.ServiceRegex;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "/api/v1")
public class controller {

    //              http://localhost:9090/api/v1/schedule/addschedule
    @PostMapping(path = "/sentence")
    @ResponseStatus(code = HttpStatus.CREATED)
    public String addSentence(@Valid @RequestBody JsonForm jsonForm) throws URISyntaxException {
        Config config = new Config();
        ClientWebSocket c = new ClientWebSocket(new URI( config.getUrlServerWebSocket() ) );
        c.makeConnection(jsonForm.getSentence());
        return "ok";
    }


    //              http://localhost:9090/api/v1/schedule/addschedule
    @PostMapping(path = "/file")
    @ResponseStatus(code = HttpStatus.CREATED)
    public String addFile(@Valid @RequestBody JsonFormFile jsonFormFile) throws URISyntaxException, IOException {
        ServiceRegex serviceRegex = new ServiceRegex();
        String[] filesSplitted = serviceRegex.splitFiles(jsonFormFile.getFile());
        ClientFileSend fileClient = new ClientFileSend(filesSplitted);
        return "ok";
    }
}
