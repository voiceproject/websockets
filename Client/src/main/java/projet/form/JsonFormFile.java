package projet.form;

import javax.validation.constraints.NotNull;

public class JsonFormFile {

    @NotNull
    private String file;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
