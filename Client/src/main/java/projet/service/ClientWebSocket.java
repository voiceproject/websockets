package projet.service;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import projet.config.Config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;


public class ClientWebSocket extends WebSocketClient {

    public ClientWebSocket(URI link){
        super(link);
    }

    public void makeConnection(String sentenceToServer){
        Config config = new Config();
        System.out.println("Your sentence : " + sentenceToServer);

        try{
            Extract extract = new Extract();
            extract.saveStr(sentenceToServer);
            ClientWebSocket c = new ClientWebSocket(new URI( config.getUrlServerWebSocket() ));
            c.connect();
        }catch (Exception e){
            System.err.println(e);
            System.exit(1);
        }
    }

    @Override
    public void onOpen( ServerHandshake handshakedata ) {

        Extract extract = new Extract();

        try {
            System.out.println( "opened connection, message to send : " +  extract.receiveStr());
            send(extract.receiveStr());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // if you plan to refuse connection based on ip or httpfields overload: onWebsocketHandshakeReceivedAsClient
    }

    @Override
    public void onMessage(String s) {
        System.out.println("Your message have been received.");
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        System.out.println(s);
    }

    @Override
    public void onError(Exception e) {
        System.out.println(e);
    }


}
