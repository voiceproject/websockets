package projet.service;

import projet.config.Config;

import java.io.*;
import java.net.Socket;

public class ClientFileSend {

    private static Socket sock;

    public ClientFileSend(String[] filesToSend) throws IOException {
        Config config = new Config();
        System.out.println("Opération en cours ... Veuillez attendre.");
        for (int i = 0; i < filesToSend.length; ++i) {
            try {
                System.out.println("Connexion au server file en cours ...");
                sock = new Socket(config.getIpServerSendFiler(), config.getPortServerSendFiler());
                BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Connexion réussite !");
                PrintStream os = new PrintStream(sock.getOutputStream());
                sendfile(filesToSend[i],i,filesToSend.length);
            } catch (Exception e) {
                System.err.println("Cannot connect to the server, try again later.");
            }

        }

    }

    private void sendfile(String fileToSend, int i,int lenght) {
        try {
            Config config = new Config();
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            System.out.println("Recherche du fichier " + fileToSend + " en cours dans la location : " + config.getLocationPackageFiles()+ "  ..");
            File myFile = new File(config.getLocationPackageFiles() + fileToSend);
            byte[] mybytearray = new byte[(int) myFile.length()];
            if(!myFile.exists()) {
                System.out.println("Le fichier que vous avez donné n'existe pas ou n'a pas été trouvé ..");
                return;
            }

            FileInputStream fis = new FileInputStream(myFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            //bis.read(mybytearray, 0, mybytearray.length);

            DataInputStream dis = new DataInputStream(bis);
            dis.readFully(mybytearray, 0, mybytearray.length);

            OutputStream os = sock.getOutputStream();

            //Sending file name and file size to the server
            DataOutputStream dos = new DataOutputStream(os);
            dos.writeUTF(myFile.getName());
            dos.writeLong(mybytearray.length);
            dos.write(mybytearray, 0, mybytearray.length);
            dos.flush();
            System.out.println("Le fichier : "+myFile.getName()+" a été envoyé au server.");
            dis.close();
            this.deleteAFile(myFile);
        } catch (Exception e) {
            System.err.println("Une erreur est survenue: "+e);
        }
    }

    private void deleteAFile(File myFile){
        System.out.println("Delete du fichier en local en cours ...");
            if(myFile.delete())
            {
                System.out.println("Le fichier a bien été détruit.");
            }
            else
            {
                System.out.println("Erreur lors de la suppression du fichier.");
            }
        }

}
