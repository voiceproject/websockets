package projet.service;

import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.*;


@Service
public class Extract {

    private ResourceLoader resourceLoader;

    public void saveStr(String strToTxt) throws IOException {

        PrintWriter writer = new PrintWriter("dataString.txt", "UTF-8");
        writer.println(strToTxt);
        writer.close();
}

    public String receiveStr() throws IOException {
        File f = new File("dataString.txt");
        FileReader fileReader = new FileReader(f);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String myMessage =  bufferedReader.readLine();
        return myMessage;
    }

}
